import { useAuth } from "~~/store";
export default defineNuxtRouteMiddleware((to, from) => {
    const auth = useAuth()
    if (!auth.isLogged) {
        return navigateTo({ path: "/" });
    }
})