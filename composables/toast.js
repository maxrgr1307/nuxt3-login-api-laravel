import { useAuth } from "~~/store"
const useToast = async (titulo = 'Mensaje', nombre = '', type = 'success', time = 3) => {
    const data = { isTitulo: titulo, isNombre: nombre, isType: type }
    const toast = useAuth()
    toast.isToast = { ...data, ...{ isActive: true} }

    const numero = ref(time)

    const temporizador = () => {
        if (numero.value == 0) {
            toast.isToast = { ...data, ...{ isActive: false } }
        } else {
            setTimeout(() => {
                numero.value--
                countDownTimer()
            }, 1000)
        }
    }

    const countDownTimer = () => {
        temporizador()
    }
    countDownTimer()
}

export default useToast