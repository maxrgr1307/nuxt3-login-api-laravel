import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    head: {
        title: 'academia viva',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'hola mundito' }
        ],
        // link: [
        //     { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        // ]
    },
    buildModules: ['@pinia/nuxt'],
    css: [
        '@/assets/css/tailwind.css',
    ],
    build: {
        postcss: {
            postcssOptions: {
                plugins: {
                    tailwindcss: {},
                    autoprefixer: {},
                },
            },
        },
    },
    runtimeConfig: {
        apiSecret: '',
        public: {
            apiBase: 'https://laravel-api-v1.herokuapp.com/api', // Or a default value
        }
    },
})
