import { defineNuxtPlugin } from '#app'
import { useAuth } from '@/store'

export default defineNuxtPlugin(async (nuxtApp) => {
  const todos = useAuth(nuxtApp.$pinia)
  await todos.getUser()
})