export const useEndpoint = async (method = 'Get', endpoint = '', data = '', type = 'gpDefault') => {
    const userToken = useCookie('Token')
    const url = `${useRuntimeConfig().public.apiBase}/${endpoint}`
    const headersConfig = { 'Content-Type': 'application/json', 'Accept': 'application/json'}
    const headersConfigToken = { ...headersConfig, ...{'Authorization': `Bearer ${userToken.value}`} }

    const configFetch = {
        confD: { "method": method, "body": JSON.stringify(data), "headers": type !='gpDefault'? headersConfigToken : headersConfig },
        confS: { "method": method, "headers": headersConfigToken },
    }

    const endpointDef = {
        'gpDefault': configFetch.confD,
        'gpNot': configFetch.confS,
        'gpwith': configFetch.confD,
    }
    
    const pathCondif = endpointDef[type]
    const res = await fetch(url, pathCondif);
    const response = await res.json();
    return {
        response
    }
}